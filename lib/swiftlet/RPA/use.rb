require 'rubygems'
require 'treetop'
require 'pp'
require File.expand_path(File.join(File.dirname(__FILE__), 'node'))
Treetop.load 'rpa'

def check_parse(str)
  parser = RPAParser.new
  parse_tree = parser.parse(str)
  if !parse_tree
    puts parser.failure_reason
    puts parser.failure_line
    puts parser.failure_column
  else
    pp parse_tree
    pp parse_tree.eval
  end
end

def check_parse2(str)
  if str.respond_to? :read
    str = str.read
  end

  parser = RPAParser.new
  parse_tree = parser.parse(str)

  if parse_tree
    # parse_tree.do_something_useful
    pp parse_tree
    pp parse_tree.eval
  else
    parser.failure_reason =~ /^(Expected .+) after/m
    puts "#{$1.gsub("\n", '$NEWLINE')}:"
    puts str.lines.to_a[parser.failure_line - 1]
    puts "#{'~' * (parser.failure_column - 1)}^"
  end
end

# ------------------------------------------------------#

# try your string here
text = %{b=1
if a==1
  log("debug", "1")
  log("debug", "2")
end
log("debug", "3")}

text = %{{"a" => "text1", "b" => "text2"}}




# check_parse(text)
check_parse2(text)



