# frozen_string_literal: true

require 'pp'

module RPA
  class BinaryOperation < Treetop::Runtime::SyntaxNode
    def eval(env = {})
      return tail.elements.inject(head.eval(env)) do |value, element|
        element.operator.apply(value, element.operand.eval(env))
      end
    end
  end

  class TernaryOperation < Treetop::Runtime::SyntaxNode
    def eval(env = {})
      head.eval(env) ? tail.operand_1.eval(env) : tail.operand_2.eval(env)
    end
  end
end
