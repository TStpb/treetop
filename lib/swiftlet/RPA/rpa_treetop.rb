require 'treetop'
require File.join(File.dirname(__FILE__), 'node.rb')
require File.join(File.dirname(__FILE__), 'rpa.rb')

class RpaTreetop < Treetop::Runtime::CompiledParser
  include RPA

  def let_parse(text)
    if parse(text).nil?
      return false
    else
      return parse(text).eval()
    end
  end

end
