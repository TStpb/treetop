dir = File.dirname(__FILE__)
require File.expand_path("#{dir}/test_helper")

require File.expand_path(File.join(File.dirname(__FILE__), 'node'))
Treetop.load File.expand_path("#{dir}/rpa")

class RPAParserTest < Test::Unit::TestCase
  include ParserTestHelper

  def setup
    @parser = RPAParser.new
  end

  def should_be_successful(str)
    assert !@parser.parse(str).nil?
  end

  def should_be_failed(str)
    assert @parser.parse(str).nil?
  end

  #if parse success return SyntaxNode
  #if parse fail return nil

  def test_numeric
    should_be_successful %{1}
    should_be_successful %{100}
    should_be_failed %{1    }
    should_be_failed %{100      }
    #  should_be_successful '0'
  end

  def test_variable
    should_be_successful %{a}
    should_be_successful %{cat}
    should_be_successful %{Asd}
    should_be_successful %{Q2q23123sdfs1}
    should_be_successful %{_2q23123sdfs1}
    should_be_failed %{1a}
    should_be_failed %{!a}
    should_be_failed %{!!@!@!@!@!@$@$#$#%#%^!}
  end

  def test_class_name
    should_be_successful %{ModuleRelationship}
    should_be_successful %{SwiftletErp}
    should_be_successful %{SwiftletErp::Module}
  end

  def test_string
    should_be_successful %{\"a\"}
    should_be_successful %{\'a\'}
    should_be_successful %{`a`}
    should_be_successful %{\"See_NAmE\"}
    should_be_successful %{\"See NAmE\"}
    should_be_successful %{\"See NAmE 1 1 1 1 adsads a1 f4\"}
    should_be_successful %{\"a\"\n\"a\"}
  end

  def test_assign_variable
    should_be_successful %{a=1}
    should_be_successful %{cat=1}
    should_be_successful %{Cat=1}
    should_be_successful %{cAt_1=1}
    should_be_successful %{f1g2=1}
    should_be_successful %{_=1}
    should_be_successful %{a    =  1}
    should_be_successful %{a = \'1\'}
    should_be_successful %{a = \'eiieieii\'}
    should_be_successful %{a = \'a a \'}
    should_be_successful %{a = var}
    should_be_successful %{a = var}
    should_be_successful %{a = true}
    should_be_successful %{a = True}
    should_be_successful %{a = false}
    should_be_failed %{1=1}
    should_be_failed %{"a"=1}
    should_be_failed %{1 = a}
    should_be_failed %{var = unless}
    should_be_failed %{1 = unless}
    should_be_failed %{true = if}
    #  should_be_successful %{a<<1}
  end

  # def test_MLHS_eq_MRHS
  #    should_be_successful %{a, b = 1, 1;}
  #    should_be_successful %{a, b = \"1\", 1;}
  #    should_be_successful %{a, b = *1;}
  #    should_be_successful %{a, b = c, e;}
  #    should_be_successful %{a, *c = 1, e;}
  # end

  def test_OP_ASGN
    should_be_failed %{a?=1}
    should_be_failed %{nil? = 1}
    should_be_successful %{a=1}
    should_be_successful %{a=a+1}
    should_be_successful %{a=1+1}
    should_be_successful %{a=1+1+1+1+1+1+1+1+222222222222}
    should_be_successful %{a = a + 1}
    should_be_successful %{a += 1}
    should_be_successful %{a+=1}
    should_be_successful %{a &&= 1}
    # and all in OP_ASGN should be correct
    should_be_successful %{a += b}
    should_be_successful %{a += \"qeqwqeww\"}
    should_be_successful %{a += \"qeqw qeww\"}
  end

  # def test_RANGE
  #    should_be_successful %{1..10;}
  #    should_be_successful %{1 .. 10;;}
  #    should_be_successful %{1...10;}
  #    should_be_successful %{1 ... 10;}
  #    should_be_successful %{a...b;}
  # end

  def test_comment
    should_be_successful %{# aaaaaaaaa}
    should_be_successful %{# # ##### aaaaaaaaa}
    should_be_successful %{# aaa aaaa aa}
    should_be_successful %{# HJDJSHWADGhasADKADSHk}
    should_be_successful %{# aaaaaaaaa\neiei=1\n  a=3}
    should_be_successful %{# @adjust_stocking = @goods_not_shipped + @adjust_stocking}
    should_be_successful %{# def sum\n#   return 0\n# end}
  end

  def test_equal_sign
    should_be_successful %{a=1+1||2&&5}
    should_be_successful %{a=1||5+6}
    should_be_successful %{a=1||q+6}
    should_be_successful %{a=a + 3 || 1 + 6}
    should_be_successful %{a=a+3|| 1 +6}
  end
  
  def test_comparative
    should_be_successful %{a==1}
    should_be_successful %{a===1}
    should_be_successful %{a>=1}
    should_be_successful %{a>1}
    should_be_successful %{a<1}
    should_be_successful %{a < 1}
    should_be_successful %{a<=1}
    should_be_successful %{a==1+1}
    should_be_successful %{a == 1 + 2}
    should_be_successful %{1&&2+1||5+3+a==2+3+3+3||+6+6}
    should_be_successful %{adasdad &&2+1||5+ 3+   a    == 2+        3+3+3||+6+6}
    should_be_successful %{a==1\nb==2}
    should_be_successful %{a==1;    a==q}
    should_be_successful %{a||b < c&&b}
    
    should_be_failed %{a << 1}
    should_be_failed %{a<<1}
    should_be_failed %{a><1}
  end

  def test_global_variable
    should_be_successful %{@a}
    should_be_successful %{@cat}
    should_be_successful %{@Asd}
    should_be_successful %{@Q2q23123sdfs1}
    should_be_successful %{@_2q23123sdfs1}
    should_be_successful %{@reserve_and_reservation_item}
    should_be_failed %{@1a}
    should_be_failed %{@!a}
    should_be_failed %{@!!@!@!@!@!@$@$#$#%#%^!}
  end


  def test_parenthesis
    should_be_successful %{a=(1+1)}
    should_be_successful %{a=(1+1)+1}
    should_be_successful %{a=1+(1+1)+1}
    should_be_successful %{a=1+1+1}
    should_be_successful %{a = ( 1 + 1 )}
    should_be_successful %{a = ( 1 + 1 ) + 1}
    should_be_successful %{a = 1 + ( 1 + 1 ) + 1}
    should_be_successful %{a = 1 + 1 + 1}
    should_be_successful %{@adjust_stocking = @goods_not_shipped + @adjust_stocking}
    should_be_successful %{a=((1+1)+(1+1))}
    should_be_successful %{a=(1+(1+1)+((1+1)))+1}
    should_be_successful %{a=((((((((((((((((1+1))))))))))))))))}
    should_be_failed %{a=((1+1)+(1+1))))))))))))))}
  end

  def test_variable_dot_medthod_lhs
    should_be_successful %{a.a = 1}
    should_be_successful %{a.a = true}
    should_be_successful %{parser.save = 1}
    should_be_successful %{parser.save = true}
    should_be_successful %{@parser.save = true}
  end


  def test_lhs_array
    should_be_successful %{a=1}
    #  should_be_successful %{a[]=1}
    #  should_be_successful %{a[[]]=1}
    #  should_be_successful %{@a[[[]]]=1}
    should_be_successful %{a[a]=1}
    should_be_successful %{a[\"a\"]=1}
    should_be_successful %{a['a']=1}
    should_be_successful %{a[a.a]=1}
    should_be_successful %{a[a][a]=1}
    should_be_successful %{a[0][0]=1}
    should_be_successful %{a[1000][978]=1}
    should_be_successful %{a[1][a0]=1}
    should_be_successful %{a[a.a][a.a]=1}
    should_be_successful %{@a[a.a][a.a]=1}
    should_be_successful %{@current_record['ready_for_delivery'] = true}
    should_be_successful %{so_item_record_pair_ppb_record[index]['po_item_record'] = po_item_record}
    should_be_successful %{a[a]=1}
    should_be_failed %{a[][]=1}
    should_be_failed %{a[00000][000010122]=1}
    should_be_failed %{@a[]=1}
  end


  def test_primary_func_call
    should_be_successful %{a}
    should_be_successful %{a()}
    should_be_successful %{a(p)}
    should_be_successful %{a(p1, p2, p3)}
    should_be_successful %{a ( p1,  p2 , p3    )}
    should_be_successful %{a.save}
    should_be_successful %{@a.save}
    should_be_successful %{@a}
    should_be_successful %{@a.save.reload}
    should_be_successful %{a.save.reload.first.nill?.blank?}
    should_be_successful %{a.save()}
    should_be_successful %{@a.save()}
    should_be_successful %{a.save(data)}
    should_be_successful %{a.save( data )}
    should_be_successful %{a.save("str")}
    should_be_successful %{a.save("str", \'string\')}
    should_be_successful %{a.save(p1, p2)}
    should_be_successful %{a.save(data, "str")}
    should_be_successful %{b = a.save}
    should_be_successful %{b = a.save()}
    should_be_successful %{b = a.save(data)}
    should_be_successful %{b = a.save(data1, data2)}
    should_be_successful %{a.all.first}
    should_be_successful %{a.all.first.present?}
    should_be_successful %{Module.where()}
    should_be_successful %{Module.where().first}
    should_be_successful %{SwiftletErp::Module.where().first}
    should_be_successful %{SwiftletErp::Module.where(id: 'str').first}
    should_be_successful %{SwiftletErp::Module.where(id: str).first}
    should_be_successful %{SwiftletErp::Module.where(id: animal.cat).first}
    should_be_successful %{SwiftletErp::Module.where(id: animal.bark('cat')).first}
    should_be_successful %Q{eiei = SwiftletErp::Module.where(id: '1150').first.name.to_s.nil?.true?}
  end

  def test_log
    should_be_successful %{log("debug", "hello world")}
    should_be_successful %{log("fatal", "hello world")}
    should_be_successful %{log('debug', "hello world")}
    should_be_successful %{log('debug', 'hello world')}
    should_be_successful %{log ("debug","hello world")}
    should_be_successful %{log ("debug","!@#$%^&*0")}
    should_be_successful %{log ("debug","สวัสดีครับ")}
    should_be_successful %{log ("debug","Hello World!!!!!!")}
    should_be_successful %{log ("debug","Hello World!!!!!!")}
    should_be_successful %{log ("debug","Hello World!!!!!!")}
    should_be_successful %{var=1\nlog ("debug", var)}
    should_be_successful %{var=1\nlog ("debug", var)}
    should_be_successful %{var=1\n\n\n\n\nlog ("debug", var)}

    #  should_be_failed %{log("Debug", "hello world")} # ไปซ้ำกับcallfunction
    should_be_failed %{log('debug' , "Hello World")}
    should_be_failed %{log('debug' , "Hello World")}
  end

  # def test_ifelse
  #   should_be_successful %{if a\n}
  #   should_be_successful %{if var\n}
  #   should_be_successful %{if iff\n}
  #   should_be_successful %{if iif\n}
  #   should_be_successful %{if true\n}
  #   should_be_successful %{if false\n}
  #   should_be_successful %{if a==1\n}
  #   should_be_successful %{if a == 1+2\n}
  #   should_be_successful %{if a==b\n}
  #   should_be_successful %{if a.save\n}
  #   should_be_successful %{if a.reload.nil?\n}
  #   # should_be_successful %{if quantity_not_shipped > 0 && a > 0\n}
  #   # should_be_successful %{if a[0]\n}
  #   # should_be_successful %{if a_r[0].nil?\n} ยังไม่ได้ทำarr ด้านขวา
  #   should_be_successful %{if }
  #   should_be_successful %{if \n}
  #   should_be_failed %{IF var\n}
  #   should_be_failed %{If var\n}
  #   should_be_failed %{iF var\n}
  #   should_be_failed %{if if\n}
  # end

  def test_define_hash
    should_be_successful %{{}}
    should_be_successful %{{ }}
    should_be_successful %{{                  }}
    should_be_successful %{{"a" => "text"}}
    should_be_successful %{{"a"=>"text"}}
    should_be_successful %{{"a" => "text1", "b" => "text2"}}
    should_be_successful %{{"a" => 1, "b" => 2}}
    should_be_successful %{{'a' => 1, 'b' => 2}}
    should_be_successful %{{"a" => var}}
    should_be_successful %{{"a" => Car.door}}
    should_be_successful %{{"a" => @Car.door}}
    # should_be_successful %{{"a" => [1, 2, 3]}} #ยังไมไ่ด้ทำ array right hand
    should_be_successful %{{"a" => 'str'}}
    should_be_successful %{{\n\t"a"=>"text"\n}}
    should_be_successful %{{\n\t"a"=>"text",\n"b" => "text2"\n}}
    should_be_successful %{{\n\t"a"=>"text", \n\t"b" => "text2"\n}}
    should_be_successful %{{\n\t"a"=>"text", \n\n\n\n\n\n\n\t"b" => "text2"\n}}
    
    should_be_successful %{a = {}}
    should_be_successful %{a = {"a" => "text"}}
    should_be_successful %{a = {"a" => "text1", "b" => "text2"}}
    should_be_successful %{a = {\n\t"a" => "text1",\n\t"b" => "text2"\n}}
    should_be_successful %{a = {\n\t"a" => "text1",\n\t"b" => "text2"\n}}
    
    should_be_failed %{{"a" => "text",}}
    should_be_failed %{{"a" => "text"\n,}}
    should_be_failed %{{\n\t"a"=>"text", \n\t"b" => "text2",\n}}
    should_be_failed %{{a: 1}}
    should_be_failed %{{:a :1}}
    should_be_failed %{{"a": 1}}
    should_be_failed %{{"a": 1}}
    should_be_failed %{{"a" = 1}}
    should_be_failed %{{"a" "1" "b"}}
    should_be_failed %{{"a" "1" "b" "2"}}
    should_be_failed %{{"a" => 1 "b" => 2}}
    should_be_failed %{{"a": 1;}}
    should_be_failed %{{"a": 1, a: 'b'}}
    should_be_failed %{{"a" =>  1, a: 'b'}}
    should_be_failed %{a = {"a" = 1}}
  end
end