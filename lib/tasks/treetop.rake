namespace :treetop do
  # task(:gen_treetop, [:rule] => [:single_preparations]) do |_, args|
  #   puts 'Generate Treetop rule file...'

  #   rpa_dir = "lib/swiftlet/RPA"

  #   Dir.glob('lib/swiftlet/RPA/*.treetop').each do|f|
  #     `tt #{rpa_dir}/#{File.basename(f)}`
  #   end


  #   ['compound', 'functionality', 'single'].each do |sub_dir|
  #     rpa_sub_dir = "lib/swiftlet/RPA/#{sub_dir}"
  #     Dir.glob("#{rpa_sub_dir}/*.treetop").each do|f|
  #       `tt #{rpa_sub_dir}/#{File.basename(f)}`
  #     end
  #   end
  # end

  task(:compile) do 
    rpa_dir = "lib/swiftlet/RPA"
    puts 'Compiling .treetop files into Ruby files...'
    all_file = Dir["#{rpa_dir}/**/*.treetop"].to_a
    all_file.each do |f|
      # puts "Compiling... #{f}"
      `tt #{f}`
    end
  end

end

# path = "/path/to/xyz.mp4"
# File.basename(path)         # => "xyz.mp4"
# File.extname(path)          # => ".mp4"
# File.basename(path, ".mp4") # => "xyz"
# File.basename(path, ".*")   # => "xyz"
# File.dirname(path)          # => "/path/to"